#include <iostream>
#include <string>
#include <alloca.h>
using namespace std;
using namespace std::literals;

struct Person {
    string   name;
    unsigned age;
    Person* next;

    Person(string name, unsigned int age, Person* next = nullptr)
            : name(move(name)), age(age), next(next) {}
};

void print(Person* link) {
    if (link == nullptr) return;
    cout << "Person{" << link->name << ", " << link->age << "} @ " << link << endl;
    print(link->next);
}

void base(unsigned N) {
    cout << "-- base(" << N << ") --\n";
    Person* head = nullptr;
    for (auto k = 1U; k <= N; ++k) {
        head = new(alloca(sizeof(Person))) Person{"nisse-"s + to_string(k), (5 + (k * 5 % 80)), head};
    }
    print(head);
}

int main() {
    base(5);
    base(10);
    base(5);
    return 0;
}
