#include <iostream>
#include <tuple>
#include <map>
#include <string>

using namespace std;

auto fib(int n) {
    if (n == 0) return 0U;
    if (n == 1) return 1U;
    return fib(n - 2) + fib(n - 1);
}

auto compute(int n) {
    return make_tuple(n, fib(n));
}

auto populate(int n) {
    auto      values = map<int, unsigned>{};
    for (auto k      = 1; k <= n; ++k) {
        auto[a, f] = compute(k);
        values[a] = f;
    }
    return values;
}

int main(int argc, char** argv) {
    auto N      = (argc == 1) ? 10 : stoi(argv[1]);
    auto result = populate(N);
    for (auto[arg, res] : result) cout << arg << " -> " << res << endl;

    return 0;
}
