#include <iostream>
#include <string>
#include "each-line.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int main(int argc, char** argv) {
    auto filename = "../each-line.test.cxx"s;
    auto lineno   = 1;
    eachLine(filename, [&lineno](auto line) {
        cout << lineno++ << ") " << line << endl;
    });
    return 0;
}

