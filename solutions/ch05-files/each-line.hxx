#pragma once

#include <fstream>
#include <string>
#include <functional>
#include <stdexcept>

namespace ribomation {
    using std::string;
    using std::function;
    using std::ifstream;
    using std::invalid_argument;
    using namespace std::string_literals;

    void eachLine(const string& filename, const function<void(const string&)>& handle) {
        auto file = ifstream{filename};
        if (!file) {
            throw invalid_argument{"cannot open "s + filename};
        }

        for (string line; getline(file, line);) handle(line);
    }

}
