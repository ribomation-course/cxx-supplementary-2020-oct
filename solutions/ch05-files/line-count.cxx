#include <iostream>
#include <iomanip>
#include <filesystem>
#include <string>
#include <unordered_set>
#include <stdexcept>
#include "each-line.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;
namespace fs = std::filesystem;

int main(int argc, char** argv) {
    auto dir = fs::path{(argc == 1) ? "../" : argv[1]};
    if (!fs::is_directory(dir)) {
        throw invalid_argument{"not a directory "s + dir.string()};
    }

    auto const EXTs   = unordered_set<string>{".txt"s, ".cxx"s, ".hxx"s,};
    auto       isText = [&EXTs](const fs::directory_entry& e) -> bool {
        if (!e.is_regular_file()) return false;
        return EXTs.count(e.path().extension()) != 0;
    };

    auto total = 0U;
    for (auto& entry : fs::recursive_directory_iterator{dir}) {
        if (isText(entry)) {
            auto lines = 0U;
            eachLine(entry.path().string(), [&lines](auto) { ++lines; });
            cout << setw(4) << right << lines << " " << entry.path() << endl;
            total += lines;
        }
    }
    cout << setw(4) << right << total << " TOTAL" << endl;

    return 0;
}
