#include <iostream>

using namespace std;

struct Data {
    unsigned id;
    Data* next = nullptr;
    long double bulk[1024]{};

    Data(unsigned int id, Data* next) : id(id), next(next) {}
};

void dispose(Data* head);
void print(Data* head);

int main(int argc, char** argv) {
    unsigned N         = (argc > 1) ? stoi(argv[1]) : 10U;
    bool     noLeakage = (argc > 2) && "true"s == argv[2];

    Data* head = nullptr;
    for (auto k = 1U; k <= N; ++k) head = new Data{k, head};
    print(head);
    if (noLeakage) dispose(head);

    return 0;
}

void print(Data* head) {
    if (head == nullptr) {
        cout << endl;
        return;
    }
    cout << head->id << " ";
    print(head->next);
}

void dispose(Data* head) {
    if (head == nullptr) return;
    dispose(head->next);
    delete head;
}
