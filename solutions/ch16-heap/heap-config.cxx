#include <iostream>
#include <memory>
#include <locale>
#include <malloc.h>
#include <unistd.h>

extern char end;

auto heapStart() -> unsigned long {
    return reinterpret_cast<unsigned long>(&end);
}

auto heapEnd() -> unsigned long {
    return reinterpret_cast<unsigned long>(sbrk(0));
}

auto heapSize() -> unsigned long {
    return heapEnd() - heapStart();
}

bool insideHeap(void* addr) {
    return reinterpret_cast<unsigned long>(addr) <= heapEnd();
}

const char* where(void* addr) {
    return insideHeap(addr) ? "inside heap" : "outside heap";
}

struct FreeDeleter {
    void operator()(void* ptr) {
        std::cout << "freeing addr " << reinterpret_cast<unsigned long>(ptr) << std::endl;
        free(ptr);
    }
};

using AutoFree = std::unique_ptr<char, FreeDeleter>;

const char* where(AutoFree& ptr) {
    return where(ptr.get());
}

int main(int argc, char** argv) {
    using namespace std;

    cout.imbue(locale{"en_US.utf8"});
    cout << "heap.start: " << heapStart() << endl;
    cout << "heap.end  : " << heapEnd() << endl;
    cout << "heap.size : " << heapSize() << " bytes" << endl;

    {
        int N = 24;
        cout << "-- Allocating " << N << " bytes --\n";
        auto ptr = AutoFree{(char*) malloc(N)};
        cout << "&ptr: " << reinterpret_cast<unsigned long>(ptr.get()) << endl;
        size_t usableSize = malloc_usable_size(ptr.get());
        cout << "actual: " << usableSize << " bytes" << endl;
        cout << "unused: " << (usableSize - N) << " bytes" << endl;
    }

    {
        int N = 25;
        cout << "-- Allocating " << N << " bytes --\n";
        auto ptr = AutoFree{(char*) malloc(N)};
        cout << "&ptr: " << reinterpret_cast<unsigned long>(ptr.get()) << endl;
        size_t usableSize = malloc_usable_size(ptr.get());
        cout << "actual: " << usableSize << " bytes" << endl;
        cout << "unused: " << (usableSize - N) << " bytes" << endl;
    }

    {
        cout << "-- Allocating one 100K block inside and one 200K outside the heap --\n";
        auto heapBlk = AutoFree{(char*) malloc(100 * 1024)};
        cout << "addr heap: " << reinterpret_cast<unsigned long>(heapBlk.get()) << ", " << where(heapBlk) << endl;

        auto mmapBlk = AutoFree{(char*) malloc(200 * 1024)};
        cout << "addr mmap: " << reinterpret_cast<unsigned long>(mmapBlk.get()) << ", " << where(mmapBlk) << endl;
    }

    cout << "-- Trimming the heap and reducing the MMAP THRESHOLD to 99K --\n";
    malloc_trim(0);
    mallopt(M_MMAP_THRESHOLD, 99 * 1024);

    {
        cout << "-- Allocating one 100K, now outside the heap --\n";
        auto heapBlk2 = AutoFree{(char*) malloc(100 * 1024)};
        cout << "addr: " << reinterpret_cast<unsigned long>(heapBlk2.get()) << ", " << where(heapBlk2) << endl;
    }
    malloc_stats();

    return 0;
}
