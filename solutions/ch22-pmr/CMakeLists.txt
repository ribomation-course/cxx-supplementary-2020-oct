cmake_minimum_required(VERSION 3.16)
project(ch22_pmr)

set(CMAKE_CXX_STANDARD 17)

add_executable(pmr
        no-heap.hxx
        app.cxx
        )
