#pragma once

#include <iostream>
#include <sstream>
#include <cstring>
#include "trace.hxx"

namespace ribomation {
    using namespace std;
    using namespace std::string_literals;
    using ribomation::trace::Trace;

    class Person {
        Trace t;
        char* name = nullptr;
        unsigned   age = 0;
        static int instanceCount;

        static char* copystr(const char* str) {
            if (str == nullptr) return nullptr;
            return strcpy(new char[strlen(str) + 1], str);
        }

    public:
        Person()
                : t{"Person"s, this}, name{copystr("")}, age{0} {
            ++instanceCount;
        }

        Person(const char* n, unsigned a)
                : t{"Person"s, this}, name{copystr(n)}, age{a} {
            ++instanceCount;
        }

        Person(const Person& that)
                : t{"Person"s, this}, name{copystr(that.name)}, age{that.age} {
            ++instanceCount;
        }

        ~Person() {
            --instanceCount;
            delete[] name;
        }

        string toString() const {
            ostringstream buf{};
            buf << "Person{" << (name ? name : "??") << ", " << age << "} @ " << this << " [" << instanceCount << "]";
            return buf.str();
        }

        unsigned incrAge() { return ++age; }

        static int count() { return instanceCount; }
    };

}
