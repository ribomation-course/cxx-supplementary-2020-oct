#include <iostream>
#include <vector>
#include "trace.hxx"
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

auto func(Person q) -> Person {
    auto t = Trace{"func"};
    q.incrAge();
    Trace::out() << "[func] q: " << q.toString() << endl;
    return q;
}

int main() {
    auto t = Trace{"main"};

    {
        auto tb = Trace{"inner block 1"};
        auto p = Person{"Cris P. Bacon", 27};
        Trace::out() << "[main] p: " << p.toString() << endl;
        auto q = func(p);
        Trace::out() << "# persons: " << Person::count() << endl;
    }
    Trace::out() << "# persons: " << Person::count() << endl;

    {
        auto tb = Trace{"inner block 2"};
        auto v = vector<Person>{{"Anna",  27},
                                {"Berit", 37},
                                {"Carin", 47}};
        Trace::out() << "# persons: " << Person::count() << endl;

        Trace::out() << "[main] -- copy print ----\n";
        for (auto p : v) cout << "[copy] p: " << p.toString() << endl;
        Trace::out() << "# persons: " << Person::count() << endl;

        Trace::out() << "[main] -- ref print ----\n";
        for (auto const& p : v) cout << "[ref]  p: " << p.toString() << endl;
    }
    Trace::out() << "# persons: " << Person::count() << endl;

    return 0;
}
