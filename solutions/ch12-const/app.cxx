#include <iostream>
#include "person.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation;

int main() {
    auto p = Person{"Nisse Hult"s, 42};
    cout << "p: " << p << endl;

    p.incrAge();
    cout << "p: " << p << endl;

    return 0;
}
