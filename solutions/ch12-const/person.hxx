#pragma once

#include <string>
#include <iosfwd>

namespace ribomation {
    using namespace std;
    using namespace std::string_literals;

    class Person {
        string      name;
        mutable int age;
    public:
        Person(string n, int a) : name{n}, age{a} {}

        int incrAge() const {
            return ++age;
        }

        friend auto operator<<(ostream& os, const Person& p) -> ostream& {
            return os << "Person{" << p.name << ", " << p.age << "}";
        }
    };

}

