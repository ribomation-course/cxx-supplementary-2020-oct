#include <iostream>
#include <string>
#include <regex>

using namespace std;
using namespace std::literals;

// date | ./cmake-build-debug/regex
// Sat Sep # #:#:# CEST #

int main() {
    auto re = regex{"\\d+"};
    for (string line; getline(cin, line);) {
        auto result = regex_replace(line, re, "#");
        cout << "" << result << endl;
    }
    return 0;
}
