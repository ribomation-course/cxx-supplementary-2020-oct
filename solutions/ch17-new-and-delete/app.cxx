#include <iostream>
using namespace std;

struct Data {
    unsigned v1;
    unsigned v2;

    Data(unsigned  v1, unsigned  v2) : v1(v1), v2(v2) {}
};

int main() {
    unsigned char buf[sizeof(Data)];
    auto ptr = new(buf) Data{5, 42};

    cout << "&buf[0]: " << reinterpret_cast<unsigned long>(buf) << endl;
    cout << "ptr    : " << reinterpret_cast<unsigned long>(ptr) << endl;

    cout << "ptr->v1: " << reinterpret_cast<unsigned*>(buf)[0] << endl;
    cout << "ptr->v2: " << reinterpret_cast<unsigned*>(buf)[1] << endl;

    return 0;
}
