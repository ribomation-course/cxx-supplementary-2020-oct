cmake_minimum_required(VERSION 3.14)
project(17_templates)

set(CMAKE_CXX_STANDARD 17)

add_executable(equals-test
        equals.hxx
        equals-test.cxx
        )

add_executable(stack-test
        stack.hxx
        stack-test.cxx
        )

