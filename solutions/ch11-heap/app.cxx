#include <iostream>
#include <vector>
#include "trace.hxx"
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

int main() {
    auto t = Trace{"main"};

    {
        auto tb = Trace{"inner block"};

        auto v = vector<Person*>{
                new Person{"Anna", 25},
                new Person{"Berit", 35},
                new Person{"Carin", 45},
                new Person{"Doris", 55},
        };
        Trace::out() << "# persons: " << Person::count() << endl;

        for (auto p : v) Trace::out() << p->toString() << endl;

        for (auto p : v) delete p;
        Trace::out() << "# persons: " << Person::count() << endl;
    }
    Trace::out() << "# persons: " << Person::count() << endl;

    return 0;
}
