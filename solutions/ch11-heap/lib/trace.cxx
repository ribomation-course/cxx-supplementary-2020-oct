#include <iostream>
#include "trace.hxx"
using namespace std;
using namespace ribomation::trace;

#ifndef NDEBUG
    ostream* Trace::logstream = &cout;
#else
    struct NullStream : ostream {
        NullStream()                  : basic_ios<char>{}, ostream{nullptr} {}
        NullStream(const NullStream&) : basic_ios<char>{}, ostream{nullptr} {}
    };
    static auto devNull = NullStream{};
    ostream* Trace::logstream = &devNull;
#endif

int Trace::level = 0;


