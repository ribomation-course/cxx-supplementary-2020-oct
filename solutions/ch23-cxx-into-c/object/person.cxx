#include <iostream>
#include "person.hxx"

using namespace std;

Person::Person(const char* name_, unsigned age_)
        : name{name_}, age{age_} {
    cout << "+Person{" << name << ", " << age << "} @ " << this << endl;
}

Person::~Person() {
    cout << "~Person{"<< name << "} @ " << this << endl;
}

auto Person::getName() const -> const char* {
    return name.c_str();
}

unsigned Person::getAge() const {
    return age;
}

auto newPerson(const char* name_, unsigned age_) -> Person* {
    return new Person(name_, age_);
}
