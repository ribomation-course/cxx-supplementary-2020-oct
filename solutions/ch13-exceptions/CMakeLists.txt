cmake_minimum_required(VERSION 3.14)
project(02_exceptions)

set(CMAKE_CXX_STANDARD 17)

add_executable(math-error
        math-lib.hxx
        math-lib.cxx
        app.cxx)
target_compile_options(math-error PRIVATE
        -Wall -Wextra -Werror -Wfatal-errors
        )

