#include <iostream>
#include <string>
#include <set>
#include <algorithm>
#include <cctype>

using namespace std;
using namespace std::literals;

string strip(string s) {
    s.erase(remove_if(s.begin(), s.end(), [](char ch) {
        return !isalpha(ch);
    }), s.end());
    return s;
}

int main() {
    auto        words = set<string>{};
    for (string word; cin >> word;) words.insert(strip(word));
    for (auto const& w : words) cout << w << " ";
    cout << endl;
    return 0;
}
