#include <iostream>
#include <vector>
#include "person.hxx"

using namespace std;
using namespace std::literals;
using namespace ribomation;

auto func(Person q) -> Person {
    cout << "[func] q: " << q.toString() << endl;
    q.incrAge();
    cout << "[func] q: " << q.toString() << endl;
    return q;
}

int main() {
    cout << "[main] enter\n";

    cout << "[main] -- block before ----\n";
    {
        auto p = Person{"Cris P. Bacon", 27};
        cout << "[main] p: " << p.toString() << endl;

        cout << "[main] -- func ----\n";
        auto q = func(move(p));                        // <----*
        cout << "[main] q: " << q.toString() << endl;
        cout << "[main] p: " << p.toString() << endl;  // <----*
    }
    cout << "[main] -- block after ----\n";

    cout << "[main] exit\n";
    return 0;
}
