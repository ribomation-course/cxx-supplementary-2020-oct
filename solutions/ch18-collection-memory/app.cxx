#include <iostream>
#include <list>
#include "trace-alloc.hxx"

using namespace std;
using namespace std::literals;

unsigned nextId = 1;

struct Dummy {
    unsigned id = nextId++;
};

int main() {
    cout << "sizeof(Dummy): " << sizeof(Dummy) << " bytes\n";
    cout << "sizeof(void*): " << sizeof(void*) << " bytes\n";
    cout << "sizeof(list::node): " << (2 * sizeof(void*) + sizeof(unsigned int) + sizeof(Dummy)) << " bytes\n";
    {
        auto      N   = 10U;
        auto      lst = list<Dummy>{};
        for (auto k   = 1U; k <= N; ++k) lst.push_back(Dummy{});
        cout << "content: ";
        for (auto const& x : lst) cout << x.id << " ";
        cout << endl;
    }
    printStats();
    cout << "[main] end\n";
    return 0;
}

