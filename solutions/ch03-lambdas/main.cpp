#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

int main() {
    int        numbers[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
    auto const N         = sizeof(numbers) / sizeof(numbers[0]);

    for_each(begin(numbers), end(numbers), [](auto n) {
        cout << n << " ";
    });
    cout << endl;

    auto const factor = 42;
    transform(numbers, numbers + N, numbers, [](auto n) {
        return n * factor;
    });

    for_each(numbers, numbers + N, [](auto n) {
        cout << n << " ";
    });
    cout << endl;

    return 0;
}
