#include <iostream>
#include <string>
#include <vector>

using namespace std;
using namespace std::literals;

int main() {
    vector<string> words = {"hi"s, "hello"s, "howdy"s, "wassup"s};
    for (string& w : words) cout << w << " ";
    cout << endl;

    auto  sentence = "It was a dark and silent night, suddenly there was a sound of a shot"s;
    if (int ix       = sentence.find("and"s); ix != string::npos)
        cout << "found it: " << ix << endl;
    if (int ix       = sentence.find("whatever"s); ix != string::npos)
        cout << "found it!" << endl;
    else
        cout << "not found: " << ix << endl;

    return 0;
}
