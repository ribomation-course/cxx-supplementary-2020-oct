#include <string>
#include "trace.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation::trace;

auto factorial(int n) -> int {
    auto t = Trace{"factorial("s + to_string(n) + ")"s};
    if (n == 0) return t.RETURN(0);
    if (n == 1) return t.RETURN(1);
    return t.RETURN(n * factorial(n - 1));
}

int main(int argc, char** argv) {
    auto t = Trace{"main"s};
    auto n = 5;
    auto f = factorial(n);
    t.out() << "fac(" << n << "): " << f << endl;
    return 0;
}


