#include <iosfwd>
#include <sstream>
#include <string>
#include "trace.hxx"
using namespace std;
using namespace std::literals;
using namespace ribomation::trace;

struct Person {
    Person(string name, int age)
            : name{name},
              age{age},
              t{"Person("s + name + ", "s + to_string(age) + ")"s, this} {
    }

    operator string() const {
        auto buf = ostringstream{};
        buf << "Person(" << name << ", " << age << ")";
        return buf.str();
    }

    friend auto operator <<(ostream& os, const Person& p) -> ostream& {
        return os << static_cast<string>(p);
    }

private:
    const string name;
    const int    age;
    Trace        t;
};

auto mk() -> Person {
    auto t = Trace("mk"s);
    auto p = Person{"Nisse"s, 42};
    return t.RETURN(p);
}

int main(int argc, char** argv) {
    auto t = Trace{"main"s};

    auto pers = mk();
    t.out() << "mk()  : " << pers << endl;
    return 0;
}


