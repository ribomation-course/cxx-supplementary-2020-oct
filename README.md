# C++ Supplementary
### Sep/Oct 2020

# Links
* [Installation Instructions](./installation-instructions.md)
* [Course Details](https://www.ribomation.se/courses/cxx/cxx-supplementary)

# Homework Assignments
1. [Task 1](./homeworks/hw-1/readme.md)
1. [Task 2](./homeworks/hw-2/readme.md)


# Usage of this GIT Repo
Ensure you have a [GIT client](https://git-scm.com/downloads) installed and clone this repo. 

    mkdir -p ~/cxx-course/my-solutions
    cd ~/cxx-course
    git clone <https url to this repo> gitlab

During the course, solutions will be push:ed to this repo and you can get these by
a `git pull` operation

    cd ~/cxx-course/gitlab
    git pull


# Build Solution/Demo Programs
The solutions and demo programs are all using CMake as the build tool. CMake is a cross platform generator tool that can generate makefiles and other build tool files. It is also the project descriptor for JetBrains CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources. 
What you do need have; are `cmake`, `make` and `gcc/g++` all installed. When you want to build a solution or demo; (a) First change into its project directory, then (b) run the  commands below.

    cd path/to/some/solutions/dir
    mkdir bld && cd bld
    cmake ..
    cmake --build .

The executable is now in the `./bld/` directory.

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

