#pragma once
#include <iostream>
#include <string>

namespace ribomation::util {
    using namespace std;
    using namespace std::literals;

    struct Trace {
        Trace() {
#ifndef NDEBUG
            log("block"s, "enter"s);
            ++level;
#endif
        }

        Trace(const string& name) : name{name} {
#ifndef NDEBUG
            log(name, "enter"s);
            ++level;
#endif
        }

        Trace(const string& name, void* addr) : name{name}, addr{addr} {
#ifndef NDEBUG
            log(name, "created"s, addr);
            //++level;
#endif
        }

        Trace(const Trace& that) : name{that.name}, addr{that.addr} {
#ifndef NDEBUG
            log(name, "copied"s, addr);
            ++level;
#endif
        }

        ~Trace() {
#ifndef NDEBUG
            if (addr) {
                log(name, "destroyed"s, addr);
            } else {
                --level;
                log(name, "exit"s);
            }
#endif
        }

        static ostream& out() {
#ifndef NDEBUG
            for (auto k = level; k > 0; --k) *logstream << ".";
#endif
            return *logstream;
        }

    private:
        const string name;
        const void    * addr = nullptr;
        static ostream* logstream;
        static int level;

        static void log(const string& name, const string& action, const void* const addr = nullptr) {
#ifndef NDEBUG
            ostream& out = Trace::out();
            out << "[" << name << "] " << action;
            if (addr) out << " @ " << addr;
            out << endl;
#endif
        }
    };

}
