#include <cstdio>
#include <cstdlib>
#include "trace-alloc.hxx"
using namespace std;

static long maxAllocatedBytes = 0;

void resetStats() {
    maxAllocatedBytes = 0;
}

void printStats() {
    printf("max allocation: %ld bytes\n", maxAllocatedBytes);
}

auto operator new(size_t numBytes) -> void* {
    auto addr = malloc(numBytes);

    printf("new: %ld bytes -> %p\n", numBytes, addr);
    maxAllocatedBytes += numBytes;

    return addr;
}

void operator delete(void* addr) noexcept {
    printf("delete: %p\n", addr);
    free(addr);
}

auto operator new[](size_t numBytes) -> void* {
    auto addr = malloc(numBytes);

    printf("new[]: %ld bytes -> %p\n", numBytes, addr);
    maxAllocatedBytes += numBytes;

    return addr;
}

void operator delete[](void* addr) noexcept {
    printf("delete: %p\n", addr);
    free(addr);
}

