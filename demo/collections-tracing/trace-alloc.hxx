#pragma once
#include <new>

auto operator new(size_t numBytes) -> void*;
void operator delete(void* addr) noexcept;

auto operator new[](size_t numBytes) -> void*;
void operator delete[](void* addr) noexcept;

void printStats();
void resetStats();
