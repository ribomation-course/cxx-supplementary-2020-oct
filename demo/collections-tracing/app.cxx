#include <iostream>
#include <vector>
#include "trace-alloc.hxx"

using namespace std;
using namespace std::literals;


int main() {
    {
        resetStats();
        cout << "--- single ---\n";
        auto n = new double{42};
        cout << "n: " << *n << " @ " << n << endl;
        delete n;
        printStats();
    }
    {
        resetStats();
        cout << "--- arr ---\n";
        auto const N = 5U;
        auto       a = new int[N];
        for (auto  k = 0; k < N; ++k) a[k] = k + 1;

        for (auto k = 0; k < N; ++k) cout << "a[" << k << "] = " << a[k] << endl;
        delete[] a;
        printStats();
    }
    {
        resetStats();
        cout << "--- vector n=100 ---\n";
        auto       v = vector<int>{};
        auto const N = 100U;
        for (auto  k = 0; k < N; ++k) v.push_back(k + 1);
        printStats();
    }
    {
        resetStats();
        cout << "--- vector n=100, reserve ---\n";
        auto       v = vector<int>{};
        auto const N = 100U;
        v.reserve(N);
        for (auto k = 0; k < N; ++k) v.push_back(k + 1);
        printStats();
    }
    return 0;
}

